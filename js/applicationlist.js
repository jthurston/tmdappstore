var apps;

$('#applicationListPage').bind('pageinit', function(event) {
	getAppList();
});

function getAppList() {
	$.getJSON("apps.json", function(data) {
		$('#applicationList li').remove();
		applications = data.apps;
		$.each(applications, function(index, application) {

            var isphone;
            var ispad;
            var isandroid;
            if(application.iphone==="Y"){
                isphone="images/iPhone.png"
            }else{
                isphone="images/noiPhone.png"
            };
            if(application.ipad==="Y"){
                ispad="images/ipad.png"
            }else{
                ispad="images/noiPad.png"
            };
            if(application.android==="Y"){
                isandroid="images/android.png"
            }else{
                isandroid="images/noAndroid.png"
            };

			/* $('#applicationList').append('<li><a href="applicationdetails.html?id=' + application.app + '">' + */
            $('#applicationList').append('<li><a href="' + application.link + '">' +
					'<img src="appicons/' + application.icon + '" style="top:20px; left:10px;"/>' +
					'<h3>' + application.app + '</h3>' +
                    '<p>' + application.by + '</p>' +
                    '<p><img src="' + isphone + '"/>' +
                    '<img src="' + ispad + '"/>' +
                    '<img src="' + isandroid + '"/></p>' +
					'<span class="ui-li-count">' + application.type + '</span></a></li>');
		});
		$('#applicationList').listview('refresh');
	});
}